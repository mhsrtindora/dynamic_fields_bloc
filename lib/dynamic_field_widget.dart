import 'package:fields_validate/topografias_list.dart';
import 'package:fields_validate/fields_widget.dart';
import 'package:flutter/material.dart';
import 'package:frideos/frideos.dart';
import 'bloc.dart';

class DynamicFieldsWidget extends StatelessWidget {
  final nameFieldsController = List<TextEditingController>();
  final ageFieldsController = List<TextEditingController>();
  final nameTopog = TextEditingController();
  @override
  Widget build(BuildContext context) {
    List<Widget> _buildFields(int length) {
      // Clear the TextEditingControllers lists
      nameFieldsController.clear();
      ageFieldsController.clear();
      for (int i = 0; i < length; i++) {
        final name = bloc.nameFields.value[i].value;
        final age = bloc.ageFields.value[i].value;
        nameFieldsController.add(TextEditingController(text: name));
        ageFieldsController.add(TextEditingController(text: age));
      }
      return List<Widget>.generate(
        length,
        (i) => FieldsWidget(
          index: i,
          nameController: nameFieldsController[i],
          ageController: ageFieldsController[i],
        ),
      );
    }

    return ListView(
      children: <Widget>[
        Padding(
          padding:
              const EdgeInsets.only(bottom: 5, left: 15, right: 15, top: 10),
          child: TextFormField(
            controller: nameTopog,
            decoration: InputDecoration(
              labelText: "Insira aqui o nome da topografia",
              hintText: "Insira aqui o nome da topografia",
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                color: Colors.blue,
                width: 2,
              )),
            ),
          ),
        ),
        ValueBuilder<List<StreamedValue<String>>>(
          streamed: bloc.nameFields,
          builder: (context, snapshot) {
            return Column(
              children: _buildFields(snapshot.data.length),
            );
          },
          noDataChild: const Text('NO DATA'),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            RaisedButton(
              color: Colors.green,
              child: const Text(
                'Adicionar campos',
              ), //style: buttonText),
              onPressed: bloc.newFields,
            ),
            StreamBuilder<bool>(
                stream: bloc.isFormValid.outStream,
                builder: (context, snapshot) {
                  return RaisedButton(
                    color: Colors.blue,
                    child: snapshot.hasData
                        ? snapshot.data
                            ? const Text(
                                'Enviar',
                              ) //style: buttonText)
                            : const Text(
                                'Formulário inválido',
                              ) //style: buttonText)
                        : const Text(
                            'Formulário inválido',
                          ), //style: buttonText),
                    onPressed: snapshot.hasData
                        ? () {
                            bloc.submit(nameTopog.text, ageFieldsController,
                                nameFieldsController);
                            print("ATENÇÃO!!! Dados já salvos no banco.");
                          }
                        : null,
                  );
                }),
          ],
        ),
        Padding(
          padding:
              const EdgeInsets.only(top: 15.0, bottom: 30, left: 30, right: 30),
          child: RaisedButton(
            color: Colors.blue,
            child: const Text(
              'Editar campos',
            ), //style: buttonText),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => TopografiasList()),
              );
            },
          ),
        ),
      ],
    );
  }
}
