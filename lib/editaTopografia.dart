import 'package:fields_validate/bloc.dart';
import 'package:fields_validate/fields_widget.dart';
import 'package:fields_validate/helper.dart';
import 'package:flutter/material.dart';

class EditarTopografia extends StatelessWidget {
  final dynamic idRelated;
  EditarTopografia({Key key, @required this.idRelated}) : super(key: key);

  final names = List<TextEditingController>();
  final ages = List<TextEditingController>();
  final ids = List<String>();

  final dbClient = DatabaseHelper();

  @override
  Widget build(BuildContext context) {
    print("ID relacionado");
    print(idRelated);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Editar Topografias"),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: Center(
          child: FutureBuilder<dynamic>(
            future: camposForm(idRelated),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return SingleChildScrollView(
                  child: Column(children: [
                    Column(children: snapshot.data),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        RaisedButton(
                          color: Colors.green,
                          child: const Text(
                            'Adicionar campos',
                          ),
                          onPressed: null, //() => newFields(),
                        ),
                        StreamBuilder<bool>(
                            stream: bloc.isFormValid.outStream,
                            builder: (context, snapshot) {
                              return RaisedButton(
                                color: Colors.blue,
                                child: snapshot.hasData
                                    ? snapshot.data
                                        ? const Text(
                                            'Salvar edição',
                                          ) //style: buttonText)
                                        : const Text(
                                            'Formulário inválido',
                                          ) //style: buttonText)
                                    : const Text(
                                        'Formulário inválido',
                                      ), //style: buttonText),
                                onPressed: snapshot.hasData
                                    ? () async {
                                        for (int i = 0; i < names.length; i++) {
                                          print(names[i].text);
                                          print(ages[i].text);
                                          print(ids[i]);

                                          await dbClient.updateDataSaving(
                                              names[i].text,
                                              ages[i].text,
                                              ids[i]);
                                        }
                                      }
                                    : null,
                              );
                            }),
                      ],
                    ),
                  ]),
                );
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ),
      ),
    );
  }

  Future<List> getData(idRelacionado) async {
    var dadosBanco = await dbClient.getDataEdition(idRelacionado);

    return dadosBanco;
  }

  Future camposForm(idRelacionado) async {
    //List<Widget> campos = List<Widget>();

    print("Entrando na função camposForm");

    var dadosBanco = await getData(idRelacionado);

    for (int i = 0; i < dadosBanco.length; i++) {
      final name = dadosBanco[i]['name'].toString();
      final age = dadosBanco[i]['age'].toString();
      final id = dadosBanco[i]['id'].toString();

      print(bloc.nameFields.value[i].value);
      print(bloc.ageFields.value[i].value);

      bloc.nameFields.value[i].value = name;
      bloc.ageFields.value[i].value = age;

      names.add(TextEditingController(text: bloc.nameFields.value[i].value));
      ages.add(TextEditingController(text: bloc.ageFields.value[i].value));
      ids.add(id);
    }

    return List<Widget>.generate(
      names.length,
      (i) => FieldsWidget(
        index: i,
        nameController: names[i],
        ageController: ages[i],
      ),
    );
  }
}
