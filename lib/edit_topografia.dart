import 'package:fields_validate/bloc.dart';
import 'package:fields_validate/fields_widget.dart';
import 'package:fields_validate/helper.dart';
import 'package:flutter/material.dart';

class EditTopologia extends StatefulWidget {
  final idRelated;

  const EditTopologia({Key key, this.idRelated}) : super(key: key);

  @override
  _EditTopologiaState createState() => _EditTopologiaState();
}

class _EditTopologiaState extends State<EditTopologia> {
  DatabaseHelper dbClient = DatabaseHelper();
  List<TextEditingController> names = List<TextEditingController>();
  List<TextEditingController> ages = List<TextEditingController>();
  List<String> ids = List<String>();

  List dados = List();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Editar Topografias"),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: Center(
          child: FutureBuilder<dynamic>(
            future: camposForm(widget.idRelated),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return SingleChildScrollView(
                  child: Column(children: [
                    Column(children: snapshot.data),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        RaisedButton(
                          color: Colors.green,
                          child: const Text(
                            'Adicionar campos',
                          ),
                          onPressed: null, //() => newFields(),
                        ),
                        StreamBuilder<bool>(
                            stream: bloc.isFormValid.outStream,
                            builder: (context, snapshot) {
                              return RaisedButton(
                                color: Colors.blue,
                                child: snapshot.hasData
                                    ? snapshot.data
                                        ? const Text(
                                            'Salvar edição',
                                          ) //style: buttonText)
                                        : const Text(
                                            'Formulário inválido',
                                          ) //style: buttonText)
                                    : const Text(
                                        'Formulário inválido',
                                      ), //style: buttonText),
                                onPressed: snapshot.hasData
                                    ? () async {
                                        for (int i = 0; i < names.length; i++) {
                                          print(names[i].text);
                                          print(ages[i].text);
                                          print(ids[i]);

                                          await dbClient.updateDataSaving(
                                              names[i].text,
                                              ages[i].text,
                                              ids[i]);
                                        }
                                      }
                                    : null,
                              );
                            }),
                      ],
                    ),
                  ]),
                );
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ),
      ),
    );
  }

  void newFields() {
    names.add(TextEditingController(text: 'Novo campo'));
    ages.add(TextEditingController(text: '01'));
    List<Widget>.generate(
      names.length,
      (i) => FieldsWidget(
        index: i,
        nameController: names[i],
        ageController: ages[i],
      ),
    );
    setState(() {});
  }

  Future<List> getData(idRelacionado) async {
    var dadosBanco = await dbClient.getDataEdition(idRelacionado);

    return dadosBanco;
  }

  Future camposForm(idRelacionado) async {
    //List<Widget> campos = List<Widget>();

    var dadosBanco = await getData(idRelacionado);

    for (int i = 0; i < dadosBanco.length; i++) {
      final name = dadosBanco[i]['name'].toString();
      final age = dadosBanco[i]['age'].toString();
      final id = dadosBanco[i]['id'].toString();

      names.add(TextEditingController(text: name));
      ages.add(TextEditingController(text: age));
      ids.add(id);
    }

    print(names.length);
    for (int j = 0; j < names.length; j++) {
      print(names[j]);
      print(names[j].text);
    }

    return List<Widget>.generate(
      names.length,
      (i) => FieldsWidget(
        index: i,
        nameController: names[i],
        ageController: ages[i],
      ),
    );
  }
}
