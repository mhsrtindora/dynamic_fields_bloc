import 'package:flutter/material.dart';

import 'bloc.dart';

class FieldsWidget extends StatelessWidget {
  const FieldsWidget({
    this.index,
    this.nameController,
    this.ageController,
  });

  final int index;
  final TextEditingController nameController;
  final TextEditingController ageController;

  @override
  Widget build(BuildContext context) {
    print("CLASSE FIELDS WIDGET");
    print(index);
    print(nameController.text);
    print(ageController.text);

    return Row(
      children: <Widget>[
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Text('${index + 1}:')),
        Expanded(
          child: Column(
            children: <Widget>[
              StreamBuilder<String>(
                  initialData: ' ',
                  stream: bloc.nameFields.value[index].outStream,
                  builder: (context, snapshot) {
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 10,
                          ),
                          child: TextField(
                            controller: nameController,
                            style: const TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              labelText: 'Nome:',
                              hintText: 'Insira um nome:',
                              errorText: snapshot.error,
                            ),
                            onChanged: bloc.nameFields.value[index].inStream,
                          ),
                        ),
                      ],
                    );
                  }),
              StreamBuilder<String>(
                  initialData: ' ',
                  stream: bloc.ageFields.value[index].outStream,
                  builder: (context, snapshot) {
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 10,
                          ),
                          child: TextField(
                            controller: ageController,
                            style: const TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              labelText: 'Idade:',
                              hintText: 'Insira a idade (1 - 130).',
                              errorText: snapshot.error,
                            ),
                            keyboardType: TextInputType.number,
                            onChanged: bloc.ageFields.value[index].inStream,
                          ),
                        ),
                      ],
                    );
                  }),
              const SizedBox(
                height: 22.0,
              ),
            ],
          ),
        ),
        IconButton(
          icon: const Icon(Icons.delete),
          color: Colors.red,
          onPressed: () => bloc.removeFields(index),
        ),
      ],
    );
  }
}
