import 'package:fields_validate/db.dart';
import 'package:sqflite/sqlite_api.dart';

class DatabaseHelper {
  String name;
  String age;

  DatabaseHelper();

  DatabaseHelper.fromMap(dynamic obj) {
    this.name = obj['name'];
    this.age = obj['age'];
  }

  Future saveData(name, age, topogRelated) async {
    Database dbClient = await DatabaseClient().db;

    //TODO: fazer um JSON aqui pra mandar as informações de nome e idade juntas?
    await dbClient.rawInsert(
        """INSERT OR REPLACE INTO Topografia_Dados(name, age, topog_related) VALUES (\'$name\', \'$age\', \'$topogRelated\') """);
  }

  Future saveTopog(name) async {
    Database dbClient = await DatabaseClient().db;

    var teste = await dbClient.rawInsert(
        """INSERT OR REPLACE INTO Topografia(topog_name) VALUES (\'$name\')""");
    return teste;
  }

  Future updateDataReceiving(newNames, newAges, idDados) async {
    for (int i = 0; i < newNames.length; i++) {
      print(newNames[i].text);
      print(newAges[i].text);
      print(idDados[i]);

      await updateDataSaving(newNames[i], newAges[i], idDados[i]);
    }
  }

  Future updateDataSaving(name, age, id) async {
    Database dbClient = await DatabaseClient().db;

    dbClient.rawUpdate("""
          UPDATE Topografia_Dados
          SET name = \'$name\'
          WHERE id = \'$id\';
        """);
    dbClient.rawUpdate("""
          UPDATE Topografia_Dados
          SET age = \'$age\'
          WHERE id = \'$id\';
        """);
  }

  Future getDataEdition(idRelacionado) async {
    Database dbClient = await DatabaseClient().db;

    var dados = await dbClient.query("Topografia_Dados",
        columns: ["id", "name", "age"],
        where: "topog_related = ?",
        whereArgs: [idRelacionado]);
    return dados;
  }

  Future getData() async {
    Database dbClient = await DatabaseClient().db;

    var dados = await dbClient.rawQuery("""SELECT * FROM Topografia_Dados""");
    return dados;
  }

  Future getTopografias() async {
    Database dbClient = await DatabaseClient().db;

    var dados = await dbClient.rawQuery("""SELECT * FROM Topografia""");
    return dados;
  }

  Future deleteTopografia(id) async {
    Database dbClient = await DatabaseClient().db;

    var delete =
        dbClient.delete('Topografia', where: "id = ?", whereArgs: [id]);
    return delete;
  }
}
