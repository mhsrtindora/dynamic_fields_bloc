import 'package:fields_validate/dynamic_field_widget.dart';
import 'package:flutter/material.dart';

import 'bloc.dart';

class DynamicFieldsPage extends StatefulWidget {
  @override
  _DynamicFieldsPageState createState() => _DynamicFieldsPageState();
}

class _DynamicFieldsPageState extends State<DynamicFieldsPage> {
  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Adicionar topografias'),
        ),
        body: DynamicFieldsWidget(),
      ),
    );
  }
}
